"===============================================================================
"
"         FILE:  .vimrc
"
"        USAGE:  sourced by vim/gvim when it is started
"
"  DESCRIPTION:  a collection of useful configurations for vim
"
"      OPTIONS:  ---
" REQUIREMENTS:  vim 7.0 or greater
"         BUGS:  ---
"        NOTES:  ---
"       AUTHOR:  Bradley Reeves (), <Brad.Reeves@gmail.com>
"      COMPANY:  Brad Reeves
"      VERSION:  3.1415
"      CREATED:  08/27/2009 09:30:09 MDT
"     REVISION:  479
"===============================================================================


"===============================================================================
"   Base Setup
"===============================================================================
"
execute pathogen#infect()
"
set nocompatible      " (compatible, cp, nocompatible, nocp) I want it to be the best VIM it can be
filetype on
syntax on             " Turn it on by default
set nospell           " Spelling is great, but not so much for code.
set si		            " smartindent
set ai	          	  " autoindent
set autoread          " autoread (ar) file when changed outside of vim
set sw=2          	  " shiftwidth = 2 spaces. The way it ought to be.
set smarttab       	  " use shiftwidth spaces when inserting tab
set bs=2 	            " (backspace) allow backspacing past where insertion began. indent,eol,start
set shiftround        " round indent to multiple of shiftwidth
set nowrap	          " do not wrap lines
set linebreak         " Wrap lines at convenient points
set expandtab         " use spaces instead of tabs
set ts=2			        " tab stops = 2 More is just wasteful. :-)
set sts=2             " softtabstop
set sta               " (stag) open tag and split window
set noeb              " the bells! ... Turn off beeps
set vb                " visual bell ... Turn this on or else the bells will keep playing
set t_vb=             " set the visual bell to nothing ...  ahhhh, peace and quite with no flashing.
set t_Co=256          " color terminal
autocmd GUIEnter * set visualbell t_vb=
set backup            "backup to ~/.tmp 
set backupdir=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp 
set backupskip=/tmp/*,/private/tmp/* 
set directory=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp 
set writebackup
set foldcolumn=1      " single column folds
set foldmethod=indent " indent for folds
set nofoldenable      " do not start with fold enabled
set mouse=a           " Use the mouse
set paste             " paste
set confirm           " confirm pop for confirmations
set mh                " (mousehide) Hide mouse pointer when typing
set fileformats=unix  " (ffs) Initial fileformat
set selectmode=mouse  " Select with the mouse
set mousem=popup      " MouseMode
set switchbuf=usetab  " use tab key to switch between buffers
set wildmenu          " command line completion enhanced mode
set wildmode=list:longest:full          "tab completion is like bash
set wildignore=*.o,*.bak,*.exe,*.class  " ignore these file types in command line completion
set ruler             " show the line and column number 
set nu                " Line numbers
set mef=/tmp/build.err " make command error file
set incsearch         " incremental search
set hlsearch          " Highlighted search
set smartcase         " smartcase
set ttyfast           " increase redraw speed in fast terminals
set title             " set the titlebar in the window running vim - default value - filename
set shell=/bin/bash\ --rcfile\ ~/.vim/.bashvimrc " what shell is used when escaped and source my .bashvimrc
set showmode          " (showmode, smd)
set showcmd           " (showcommand, sc)
set gcr=a:blinkon0    " Disable blinking cursor
set history=1000      " Store 1000 lines of command history
set viminfo='10,\"1000  " read/write a .viminfo file. only 50 lines
set hidden

"===============================================================================
" Status Line
"===============================================================================
set laststatus=2      " the last window opened has control of the status line
set statusline=%<%f\%h%m%r
set statusline+=%=\ Line=%l\ \ col=%c%V\ \ totline=%L\ \ %P
"set statusline+=%{fugitive#statusline()}
set  rtp+=/usr/local/lib/python2.7/dist-packages/powerline/bindings/vim/
set laststatus=2
set t_Co=256


"===============================================================================
" Turned off for now...
"===============================================================================
" set list               "show eol and tab characters
" set listchars=tab:>.,eol:\\
" set showmatch         " (showmatch, sm )
" set ic		            " (ignore case, ic)  ignorecase
" set statusline=%<%f\%h%m%r%=%-20.(line=%l\ \ col=%c%V\ \ totlin=%L%)\ \ \%h%m%r%=%-40(bytval=0x%B,%n%Y%)\%P
" set im               " start in Insert Mode  <C-l> to get to command mode
" make YCM compatible with UltiSnips (using supertab)
let g:ycm_key_list_select_completion = ['<C-n>', '<Down>']
let g:ycm_key_list_previous_completion = ['<C-p>', '<Up>']
let g:SuperTabDefaultCompletionType = '<C-n>'

" better key bindings for UltiSnipsExpandTrigger
let g:UltiSnipsExpandTrigger = "<tab>"
let g:UltiSnipsJumpForwardTrigger = "<tab>"
let g:UltiSnipsJumpBackwardTrigger = "<s-tab>"
"===============================================================================
"  gui (gvim) specific
"===============================================================================
"
set guioptions+=b
set guioptions-=T    " Turn off showing the Toolbar
set guitablabel=%!expand(\"\%:t\")
set guitabtooltip=%!bufname($)
if has ("gui_running")
  set lines=60          " initial number of lines for size of vim window
  set columns=140       " initial number of columns for size of vim window
  colorscheme molokai     " Looking good, Billy Ray. Feeling good, Louis. (GVIM)
  "colorscheme PaperColor     " Looking good, Billy Ray. Feeling good, Louis. (GVIM)
else
  colorscheme molokai     " Looking good, Billy Ray. Feeling good, Loius. (VIM)
  "colorscheme PaperColor     " Looking good, Billy Ray. Feeling good, Louis. (GVIM)
end

"===============================================================================
"  Font / Spelling 
"===============================================================================
"
"set guifont=Monospace:h9
"set gfn=Monospace\ 9
"set gfn=Liberation\ Mono\ 14 
set gfn=Hack\ Regular\ 16 
" if has("gui_running")
"   if has("gui_gtk2")
"      set guifont=Ubuntu\ Mono\ derivative\ Powerline\ Regular\ 18
"     elseif has("x11")
"       set guifont=-*-courier-medium-r-normal-*-*-180-*-*-m-*-*
"     else
"        set guifont=Menlo_Regular:h14
"     endif
"setlocal spell spelllang=en_us
" endif

"===============================================================================
"  FileType Support
"===============================================================================
"
autocmd FileType make  set noexpandtab
filetype on
filetype plugin on
filetype indent on
runtime! macros/matchit.vim

"===============================================================================
"  Maps
"===============================================================================
"
let mapleader = ","
" Heck yes, become root and edit this file!!
nmap <leader>z :w !sudo tee %<CR> 
nmap <leader>r :source ~/.vimrc<CR>
nmap <leader>v :e ~/.vimrc<CR>
nmap <leader>c :e ~/.vim/cheatsheet.txt<CR>
nmap <leader>e :Explore<CR>
map  <leader>n  :execute 'NERDTreeToggle ' . getcwd()<CR>
map  <leader>m  :TagbarToggle<CR>
map  <leader>s :call ToggleScratch()<CR>
map  <leader>i :w!<CR>:!irb %<CR>
map  <leader>ta :tab sball<CR>
map  <leader>yr :YRShow<CR>
map  <C-Z> :ConqueTermSplit bash<CR>
map  <C-y> :!ruby <C-r> %<CR>
nmap <leader>ln :set number<CR>
nmap <leader>lo :set nonumber<CR>
nmap <leader>h :execute "Ri " expand("<cword>")<CR>
nmap <leader>g :noautocmd :execute "vimgrep /" . expand("<cword>") . "/j **" <Bar> cw<cr>
map  <C-t> :tabnew<CR>
map  <C-left> :tabp<CR>
map  <C-right> :tabn<CR>
map  <A-o> :ProjectBrowse ~/.vim/projects\<CR>
nnoremap <C-h> :split ~/.vim/cheatsheet.txt<CR>
nnoremap <C-H> :split ~/.vim/snippets/ruby.snippets<CR>
nnoremap <C-g> :help fugitive<CR>
nnoremap gf <C-W>gf
nnoremap <leader><Left> <C-W>h
nnoremap <leader><Right> <C-W>l
nnoremap <c-s>  :SyntasticCheck<cr>
"Fugitive maps
map  <leader>gs :Gstatus<CR>
map  <leader>gd :Gdiff<CR>
map  <leader>gc :Gcommit<CR>
map  <leader>gb :Gblame<CR>
map  <leader>gl :Glog<CR>
map  <leader>gp :Git push<CR>
cmap w!! :w !sudo tee %<CR>

" Quick jumping between splits
map <C-J> <C-W>j<C-W>_
map <C-K> <C-W>k<C-W>_

" Function Keys
nnoremap <silent><F2> :NERDTree<CR>
nnoremap <F3> :shell<CR>

nnoremap <silent> <F4> :MyFoldEnable<CR>
nnoremap <F5> <C-W><C-W>
nnoremap <F6> <C-W>h<CR>:q<CR>:q<CR>
inoremap <F7> <Esc>:Tlist<CR><C-W>h<C-W>s:Explore<CR>:set nonu<CR><C-W>l
nnoremap <F8> :!ruby %<CR>
nnoremap <F9> q:
map <F10> :tabn<cr>
map <F11> :tabp<cr>
map <F12> :execute "vimgrep /" . expand("<cword>") . "/j **" <Bar> cw<CR>

"===============================================================================
"  Programming - general
"===============================================================================
"
let g:home_dir = "/Users/breeves"
syntax sync minlines=10000
set tags=/Users/breeves/.tags/tags
set tagstack
highlight Folded ctermfg=6 ctermbg=0
highlight FoldColumn ctermfg=6 ctermbg=0
highlight Pmenu guibg=blue gui=bold          " omni complete menu colors
let g:SuperTabDefaultCompletionType = "context"
set includeexpr+=substitute(v:fname,'s$','','g')
set includeexpr+=substitute(substitute(substitute(v:fname,'s$','','g'),'ie$','y','g'),'ve$','f','g')
set completeopt=longest,menuone
set com=s1:/*,mb:*,ex:*/,://,b:#,:%,:XCOMM,n:>,fb:-
set path+=lib
set path+=test
set path+=conf

"===============================================================================
"  Programming - ruby
"===============================================================================
"
set suffixesadd=.rb
autocmd FileType ruby,eruby set omnifunc=rubycomplete#Complete
autocmd FileType ruby,eruby let g:rubycomplete_buffer_loading = 1
autocmd FileType ruby,eruby let g:rubycomplete_rails = 1
autocmd FileType ruby,eruby let g:rubycomplete_classes_in_global = 1
set mp=ruby\ %
autocmd BufRead,BufNewFile Rakefile set filetype=ruby
autocmd BufRead,BufNewFile rakefile set filetype=ruby
autocmd BufRead,BufNewFile *.rake   set filetype=ruby
autocmd BufRead,BufNewFile .irbrc   set filetype=ruby
autocmd BufRead,BufNewFile *.rjs    set filetype=ruby
autocmd BufRead,BufNewFile *_spec.rb    set filetype=rspec

" ruby spec test setup  for Indra
let g:speckyRunSpecCmd = "rspec -r ~/.vim/ruby/specky_formatter.rb -f SpeckyFormatter"
let g:speckyBannerKey        = "<C-S>b"
let g:speckyQuoteSwitcherKey = "<C-S>'"
let g:speckyRunRdocKey       = "<C-S>r"
let g:speckySpecSwitcherKey  = "<C-S>x"
let g:speckyRunSpecKey       = "<C-S>s"
let g:speckyRunRdocCmd       = "fri -L -f plain"
let g:speckyWindowType       = 2

let g:rubytest_cmd_spec = "rake spec %p"
let g:rubytest_cmd_example = 'rake spec SPEC="%p" -e SPEC_OPTS="--line %c"'

iab #i include
iab #r require
" diagraphs
iab #* <C-v>u2022
iab #b <C-k>RO
iab #v <C-k>OK
iab #x <C-k>XX
imap <C-l> <Space>=><Space>"

"===============================================================================
"  Programming - JavaScript
"===============================================================================
" JavaScript can be just like Ruby
autocmd FileType javascript call UseRubyIndent()
autocmd FileType javascript set omnifunc=javascriptcomplete#CompleteJS
autocmd FileType javascript set omnifunc=javascriptcomplete#CompleteJS
autocmd FileType html set omnifunc=htmlcomplete#CompleteTags
autocmd FileType css set omnifunc=csscomplete#CompleteCSS

"===============================================================================
"  Programming - Python
"===============================================================================
autocmd FileType python set omnifunc=pythoncomplete#Complete
if executable('pyls')
    au User lsp_setup call lsp#register_server({
        \ 'name': 'pyls',
        \ 'cmd': {server_info->['pyls']},
        \ 'whitelist': ['python'],
        \ })
  endif

"===============================================================================
"  Programming - work
"===============================================================================

"===============================================================================
" File explorer
"===============================================================================
"
"Split vertically
let g:explVertical=1
"Window size
let g:explWinSize=35
let g:explSplitLeft=1
let g:explSplitBelow=1
"Hide some files
let g:explHideFiles='^\.,.*\.class$,.*\.swp$,.*\.pyc$,.*\.swo$,\.DS_Store$'
"Hide the help thing..
let g:explDetailedHelp=0

"===============================================================================
"  Functions
"===============================================================================

"===============================================================================
"  Functions - ack
"===============================================================================
let g:ackprg="ack-grep -H --nocolor --nogroup --column"

"===============================================================================
"  Functions - grep
"===============================================================================
" grep function.. get word under curser in all files under the current dir."
map \* "syiw:Grep^Rs<cr>
function! Grep(name)
  let l:pattern = input("Other pattern: ")
  "let l:_name = substitute(a:name, "\\s", "*", "g")
  let l:list=system("grep -nIR '".a:name."' * | grep -v 'svn-base' | grep '" .l:pattern. "' | cat -n -")
  let l:num=strlen(substitute(l:list, "[^\n]", "", "g"))
  if l:num < 1
    echo "'".a:name."' not found"
    return
  endif
  
  echo l:list
  let l:input=input("Which?\n")
  
  if strlen(l:input)==0
    return
  endif
  
  if strlen(substitute(l:input, "[0-9]", "", "g"))>0
    echo "Not a number"
    return
  endif
  
  if l:input<1 || l:input>l:num
    echo "Out of range"
    return
  endif
  
  let l:line=matchstr("\n".l:list, "".l:input."\t[^\n]*")
  let l:lineno=matchstr(l:line,":[0-9]*:")
  let l:lineno=substitute(l:lineno,":","","g")
  "echo "".l:line
  let l:line=substitute(l:line, "^[^\t]*\t", "", "")
  "echo "".l:line
  let l:line=substitute(l:line, "\:.*", "", "")
  "echo "".l:line
  "echo "\n".l:line
  execute ":e ".l:line
  execute "normal ".l:lineno."gg"
endfunction

command! -nargs=1 Grep :call Grep("<args>")

"===============================================================================
"  Functions - ToggleScratch
"===============================================================================
"
function! ToggleScratch()
  if expand('%') == g:ScratchBufferName
    quit
  else
    Sscratch
  endif
endfunction

"===============================================================================
"  Functions - NumberToggle()
"===============================================================================
function! NumberToggle()
  if(&relativenumber == 1)
    set number
  else
    set relativenumber
  endif
endfunc

nnoremap <C-n> :call NumberToggle()<cr>"
set runtimepath^=~/.vim/bundle/ag
"===============================================================================
"  Functions - peco open
"===============================================================================
"
function! PecoOpen()
  for filename in split(system("find . -type f | peco"), "\n")
    execute "e" filename
  endfor
endfunction
nnoremap <Leader>op :call PecoOpen()<CR>
" install: curl https://raw.githubusercontent.com/prabirshrestha/vim-lsp/master/minimal.vimrc -o /tmp/minimal.vimrc
" uninstall: rm /tmp/plug.vim && rm -rf /tmp/plugged
" run vim/neovim with minimal.vimrc
" vim -u minimal.vimrc
" :PlugInstall

set nocompatible hidden laststatus=2

" Coc stuff:
" Extensions
let g:coc_global_extensions = ['coc-json', 'coc-git', 'coc-yaml', 'coc-lists', 'coc-snippets', 'coc-go', 'coc-docker', 'coc-syntax', 'coc-dictionary']

" Use <C-l> for trigger snippet expand.
imap <C-l> <Plug>(coc-snippets-expand)

" Use <C-j> for select text for visual placeholder of snippet.
vmap <C-j> <Plug>(coc-snippets-select)

" Use <C-j> for jump to next placeholder, it's default of coc.nvim
let g:coc_snippet_next = '<c-j>'

" Use <C-k> for jump to previous placeholder, it's default of coc.nvim
let g:coc_snippet_prev = '<c-k>'

" Use <C-j> for both expand and jump (make expand higher priority.)
imap <C-j> <Plug>(coc-snippets-expand-jump)



"===
" plug-vim 
"===
" Specify a directory for plugins
call plug#begin('~/.vim/plugged')

" Make sure you use single quotes
" Shorthand notation; fetches https://github.com/junegunn/vim-easy-align
"Plug 'junegunn/vim-easy-align'

" Any valid git URL is allowed
"Plug 'https://github.com/junegunn/vim-github-dashboard.git'

" Multiple Plug commands can be written in a single line using | separators
"Plug 'SirVer/ultisnips' | Plug 'honza/vim-snippets'

" On-demand loading
"Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
"Plug 'tpope/vim-fireplace', { 'for': 'clojure' }

" Using a non-master branch
"Plug 'rdnetto/YCM-Generator', { 'branch': 'stable' }

" Using a tagged release; wildcard allowed (requires git 1.9.2 or above)
"Plug 'fatih/vim-go', { 'tag': '*' }

" Plugin options
"Plug 'nsf/gocode', { 'tag': 'v.20150303', 'rtp': 'vim' }

" Plugin outside ~/.vim/plugged with post-update hook
"Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
"
" Unmanaged plugin (manually installed and updated)
"Plug '~/my-prototype-plugin'


" COC  Use release branch
Plug 'neoclide/coc.nvim', {'branch': 'release'}
" Or latest tag
"Plug 'neoclide/coc.nvim', {'tag': '*', 'branch': 'release'}
" Or build from source code by use yarn: https://yarnpkg.com
"Plug 'neoclide/coc.nvim', {'do': 'yarn install --frozen-lockfile'}

" Initialize plugin system
call plug#end()
